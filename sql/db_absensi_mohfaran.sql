-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 03, 2023 at 11:18 PM
-- Server version: 10.4.28-MariaDB
-- PHP Version: 8.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_absensi_mohfaran`
--

-- --------------------------------------------------------

--
-- Table structure for table `absensis`
--

CREATE TABLE `absensis` (
  `absen_id` bigint(20) NOT NULL,
  `nik` bigint(20) NOT NULL,
  `jam_id` int(11) DEFAULT NULL,
  `jam_masuk` time DEFAULT NULL,
  `jam_pulang` time DEFAULT NULL,
  `tanggal_absen` date NOT NULL,
  `status_absen` varchar(255) NOT NULL,
  `file_izin` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `absensis`
--

INSERT INTO `absensis` (`absen_id`, `nik`, `jam_id`, `jam_masuk`, `jam_pulang`, `tanggal_absen`, `status_absen`, `file_izin`, `created_at`, `updated_at`) VALUES
(2, 3, 1, '10:40:03', '10:40:43', '2023-07-06', 'Terlambat', NULL, '2023-07-07 10:40:03', '2023-07-07 10:40:43'),
(4, 3, NULL, NULL, NULL, '2023-07-07', 'Izin', '1688702165_48. IPA SMP 8 S2 B10 RITA PURWANTI.doc.pdf', '2023-07-07 10:56:05', '2023-07-07 10:56:05');

-- --------------------------------------------------------

--
-- Table structure for table `dokumen_cutis`
--

CREATE TABLE `dokumen_cutis` (
  `dokumen_id` bigint(20) NOT NULL,
  `nik` bigint(20) NOT NULL,
  `tanggal_mulai_cuti` date NOT NULL,
  `tanggal_berakhir_cuti` date NOT NULL,
  `alasan_cuti` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  `tanggal_disetujui` date DEFAULT NULL,
  `alasan_ditolak` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `dokumen_cutis`
--

INSERT INTO `dokumen_cutis` (`dokumen_id`, `nik`, `tanggal_mulai_cuti`, `tanggal_berakhir_cuti`, `alasan_cuti`, `status`, `tanggal_disetujui`, `alasan_ditolak`, `created_at`, `updated_at`) VALUES
(2, 3, '2023-05-12', '2023-05-17', 'Liburan', 'Disetujui', '2023-05-10', NULL, NULL, NULL),
(4, 3, '2023-07-07', '2023-07-26', 'gagasan', 'Disetujui', '2023-07-07', NULL, '2023-07-07 04:07:32', '2023-07-07 07:38:40');

-- --------------------------------------------------------

--
-- Table structure for table `jabatan`
--

CREATE TABLE `jabatan` (
  `jabatan_id` bigint(20) NOT NULL,
  `nama_jabatan` varchar(100) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `jabatan`
--

INSERT INTO `jabatan` (`jabatan_id`, `nama_jabatan`, `created_at`, `updated_at`) VALUES
(1, 'CEO', NULL, NULL),
(3, 'Manajer', NULL, NULL),
(4, 'IT Support', NULL, NULL),
(5, 'Direktur Utama', '2023-05-10 23:04:25', '2023-05-10 23:19:12'),
(6, 'Call Center', '2023-05-10 23:18:54', '2023-05-10 23:18:54');

-- --------------------------------------------------------

--
-- Table structure for table `jam_karyawan`
--

CREATE TABLE `jam_karyawan` (
  `jam_id` bigint(20) NOT NULL,
  `jam_masuk` time NOT NULL,
  `jam_pulang` time NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `jam_karyawan`
--

INSERT INTO `jam_karyawan` (`jam_id`, `jam_masuk`, `jam_pulang`, `created_at`, `updated_at`) VALUES
(1, '08:00:00', '12:00:00', NULL, '2023-07-07 02:20:55'),
(2, '13:00:00', '16:00:00', NULL, '2023-05-10 23:46:39');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `nik` bigint(20) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `jabatan_id` bigint(20) NOT NULL,
  `jenis_kelamin` varchar(100) NOT NULL,
  `role` varchar(100) NOT NULL,
  `no_hp` varchar(100) DEFAULT NULL,
  `alamat` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`nik`, `nama`, `password`, `jabatan_id`, `jenis_kelamin`, `role`, `no_hp`, `alamat`, `created_at`, `updated_at`) VALUES
(1, 'Administrator', '$2y$10$F8K6Eyl5e/rlVS2kwzzPSu1BB7oTCrkFZyf4Emycp6w/bKRUB.zZ2', 1, 'Laki - Laki', 'Admin', '082275713049', 'da', NULL, '2023-07-07 07:40:16'),
(3, 'User 1', '$2y$10$F8K6Eyl5e/rlVS2kwzzPSu1BB7oTCrkFZyf4Emycp6w/bKRUB.zZ2', 6, 'Laki - Laki', 'Karyawan', '123', 'adsas', '2023-05-11 00:25:26', '2023-07-07 04:27:32');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `absensis`
--
ALTER TABLE `absensis`
  ADD PRIMARY KEY (`absen_id`),
  ADD KEY `user_id` (`nik`);

--
-- Indexes for table `dokumen_cutis`
--
ALTER TABLE `dokumen_cutis`
  ADD PRIMARY KEY (`dokumen_id`),
  ADD KEY `user_id` (`nik`);

--
-- Indexes for table `jabatan`
--
ALTER TABLE `jabatan`
  ADD PRIMARY KEY (`jabatan_id`);

--
-- Indexes for table `jam_karyawan`
--
ALTER TABLE `jam_karyawan`
  ADD PRIMARY KEY (`jam_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`nik`),
  ADD KEY `jabatan_id` (`jabatan_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `absensis`
--
ALTER TABLE `absensis`
  MODIFY `absen_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `dokumen_cutis`
--
ALTER TABLE `dokumen_cutis`
  MODIFY `dokumen_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `jabatan`
--
ALTER TABLE `jabatan`
  MODIFY `jabatan_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `jam_karyawan`
--
ALTER TABLE `jam_karyawan`
  MODIFY `jam_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `nik` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=123457;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `absensis`
--
ALTER TABLE `absensis`
  ADD CONSTRAINT `absensis_ibfk_1` FOREIGN KEY (`nik`) REFERENCES `users` (`nik`);

--
-- Constraints for table `dokumen_cutis`
--
ALTER TABLE `dokumen_cutis`
  ADD CONSTRAINT `dokumen_cutis_ibfk_1` FOREIGN KEY (`nik`) REFERENCES `users` (`nik`);

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_ibfk_1` FOREIGN KEY (`jabatan_id`) REFERENCES `jabatan` (`jabatan_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
