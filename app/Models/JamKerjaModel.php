<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class JamKerjaModel extends Model
{
    use HasFactory;
    protected $table = 'jam_karyawan';
    protected $primarykey = 'jam_id';
    protected $fillable = [
    	'jam_masuk',
    	'jam_pulang',
    ];
}
