<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DokumenCutiModel extends Model
{
    use HasFactory;
    protected $table = 'dokumen_cutis';
    protected $primarykey = 'dokumen_id';
    protected $fillable = [
    	'user_id',
    	'tanggal_mulai_cuti',
    	'tanggal_berakhir_cuti',
    	'alasan_cuti',
    	'status',
        'tanggal_disetujui',
        'alasan_ditolak'
    ];
}
