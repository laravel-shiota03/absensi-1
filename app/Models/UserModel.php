<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserModel extends Model
{
    use HasFactory;
    protected $table = 'users';
    protected $primarykey = 'user_id';
    protected $fillable = [
    	'user_id',
    	'nama',
    	'nip',
    	'password',
    	'jabatan_id',
    	'jenis_kelamin',
    	'role',
    	'no_hp',
    	'alamat',
    	'created_at',
    	'updated_at'
    ];
}
