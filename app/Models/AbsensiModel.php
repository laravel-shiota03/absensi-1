<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AbsensiModel extends Model
{
    use HasFactory;
    protected $table = 'absensis';
    protected $primarykey = 'absen_id';
    protected $fillable = [
    	'nik',
        'jam_id',
    	'jam_masuk',
    	'jam_pulang',
    	'tanggal_absen',
    	'jam_id',
    	'status_absen',
    	'file_izin'
    ];
}
