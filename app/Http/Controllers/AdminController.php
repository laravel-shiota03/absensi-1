<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\UserModel;
use App\Models\JabatanModel;
use App\Models\JamKerjaModel;
use App\Models\DokumenCutiModel;
use App\Models\AbsensiModel;
use Session, DB;
use Codedge\Fpdf\Fpdf\Fpdf;

class AdminController extends Controller
{
    public function dashboard(){
        $administrator = UserModel::where('role', 'Admin')->count();
        $karyawan = UserModel::where('role', 'Karyawan')->count();
        $cuti = DokumenCutiModel::where('status', 'Sedang Diproses')->count();
        $jabatan = JabatanModel::all();
        $jampagi = JamKerjaModel::where('jam_id', 1)->first();
        $jamsiang = JamKerjaModel::where('jam_id', 2)->first();
        $data = [
            'administrator' => $administrator,
            'karyawan' => $karyawan,
            'cuti' => $cuti,
            'jampagi' => $jampagi,
            'jamsiang' => $jamsiang,
            'jabatan' => $jabatan
        ];
    	return view('pages.admin.dashboard.index')->with($data);
    }
    public function administrator(){
        $admin = UserModel::join('jabatan', 'jabatan.jabatan_id', '=', 'users.jabatan_id')->where('users.role', 'Admin')->get();
        $jabatan = JabatanModel::all();
        $data = [
            'admin' => $admin,
            'jabatan' => $jabatan
        ];
    	return view('pages.admin.administrator.index')->with($data);
    }
    public function karyawan(){
        $karyawan = UserModel::join('jabatan', 'jabatan.jabatan_id', '=', 'users.jabatan_id')->where('users.role', 'Karyawan')->get();
        $jabatan = JabatanModel::all();
        $data = [
            'karyawan' => $karyawan,
            'jabatan' => $jabatan
        ];
    	return view('pages.admin.karyawan.index')->with($data);
    }
    public function absensi(){
    	return view('pages.admin.absensi.index');
    }
    public function viewabsensi(Request $request){
        $karyawan = UserModel::where('role', 'Karyawan')->get();
        $data = [
            'karyawan' => $karyawan,
            'tanggal' => $request->tanggal
        ];
    	return view('pages.admin.absensi.view')->with($data);	
    }
    public function cuti(){
         $karyawan = UserModel::join('dokumen_cutis', 'dokumen_cutis.nik', '=', 'users.nik')->get();
         $data = [
            'karyawan' => $karyawan,
        ];
    	return view('pages.admin.cuti.index')->with($data);
    }
    public function profil(){
    	$karyawan = UserModel::join('jabatan', 'jabatan.jabatan_id', '=', 'users.jabatan_id')->where('users.nik', Session::get('nik'))->first();
        $jabatan = JabatanModel::all();
        $data = [
            'karyawan' => $karyawan,
            'jabatan' => $jabatan
        ];
    	return view('pages.admin.profil.index')->with($data);
    }
    public function jabatan(){
        return redirect(route('admin.dashboard'));
    }
    public function jamKerja(){
        return redirect(route('admin.dashboard'));   
    }
    public function viewuser(Request $request){
        $karyawan = UserModel::join('jabatan', 'jabatan.jabatan_id', '=', 'users.jabatan_id')->where('users.nik', $request->id)->first();
        $jabatan = JabatanModel::all();
        $data = [
            'karyawan' => $karyawan,
            'jabatan' => $jabatan
        ];
        return view('pages.admin.user.view')->with($data);
    }
    public function showuser(Request $request){
        $karyawan = UserModel::join('jabatan', 'jabatan.jabatan_id', '=', 'users.jabatan_id')->where('users.nik', $request->id)->first();
        $jabatan = JabatanModel::all();
        $data = [
            'karyawan' => $karyawan,
            'jabatan' => $jabatan
        ];
        return view('pages.admin.user.edit')->with($data);
    }

    // Show Function
    public function showJabatan(Request $request){
        $id = $request->id;
        $data = JabatanModel::where('jabatan_id', $id)->first();
        return response()->json(['data' => $data]);
    }
    // Store Function
    public function storeJabatan(Request $request){
        if(isset($_POST['add'])){
            $jabatan = new JabatanModel;
            $jabatan->nama_jabatan = $request->nama_jabatan;
            $jabatan->save();
            if($jabatan){
                return back()->with('success', 'Data berhasil ditambahkan');
            }
        }elseif(isset($_POST['edit'])){
            $update = JabatanModel::where('jabatan_id', $request->jabatan_id)->update(['nama_jabatan' => $request->nama_jabatan]);
            if($update){
                return back()->with('success', 'Data berhasil diperbarui');
            }
        }
    }
    public function updateJamKerja(Request $request){
        if(isset($_POST['jampagi'])){
            $data = [
                'jam_masuk' => $request->jam_masuk_pagi,
                'jam_pulang' => $request->jam_pulang_pagi,
            ];
            $update = JamKerjaModel::where('jam_id', 1)->update($data);
        }elseif(isset($_POST['jamsiang'])){
            $data = [
                'jam_masuk' => $request->jam_masuk_siang,
                'jam_pulang' => $request->jam_pulang_siang,
            ];
            $update = JamKerjaModel::where('jam_id', 2)->update($data);
        }

        if($update){
            return back()->with('success', 'Data berhasil diperbarui');
        }
    }
    public function storeUser(Request $request){
        if(isset($_POST['addkaryawan']) || isset($_POST['addadministrator'])){
            $usercek = UserModel::where('nik', $request->nip)->count();
            if($usercek > 0){
                return back()->with('error', 'nik sudah pernah digunakan');
            } else {
                if(isset($_POST['addkaryawan'])){
                    $user = new UserModel;
                    $user->nama = $request->nama;
                    $user->nik = $request->nip;
                    $user->password = password_hash($request->password, PASSWORD_DEFAULT);
                    $user->jabatan_id = $request->id_jabatan;
                    $user->jenis_kelamin = $request->jenis_kelamin;
                    $user->role = 'Karyawan';
                    $user->save();
                } elseif(isset($_POST['addadministrator'])){
                    $user = new UserModel;
                    $user->nama = $request->nama;
                    $user->nik = $request->nip;
                    $user->password = password_hash($request->password, PASSWORD_DEFAULT);
                    $user->jabatan_id = $request->id_jabatan;
                    $user->jenis_kelamin = $request->jenis_kelamin;
                    $user->role = 'Admin';
                    $user->save();
                }

                if($user){
                    return back()->with('success', 'Data berhasil ditambahkan');
                }
            }
        }
    }
    public function updateuser(Request $request){
        $cekkaryawan = UserModel::where('nik', $request->niklama)->first();
        if($request->nik != $request->niklama){
            $cekkaryawan2 = UserModel::where('nik', $request->nik)->get()->count();
            if($cekkaryawan2 > 0){
                return back()->with('error', 'NIK sudah digunakan');
            }
        }
        if($request->password == $cekkaryawan->password){
            $pass = $request->password;
        } else {
            $pass = password_hash($request->password, PASSWORD_DEFAULT);
        }
        $request->session()->put('nik', $request->nik);
        $data = [
            'nama' => $request->nama,
            'nik' => $request->nik,
            'password' => $pass,
            'jabatan_id' => $request->id_jabatan,
            'jenis_kelamin' => $request->jenis_kelamin,
            'no_hp' => $request->no_hp,
            'alamat' => $request->alamat,
        ];
        $update = UserModel::where('nik', $request->niklama)->update($data);
        if($update){
            return redirect(route('admin.user').'/edit/'.$request->nik)->with('success', 'Data berhasil diupdate');
        }
    }
    // Delete Function
    public function deleteJabatan(Request $request){
        $delete = JabatanModel::where('jabatan_id', $request->id)->delete();
        if($delete){
            return back()->with('success', 'Data berhasil dihapus');
        }
    }
    public function deleteuser(Request $request){
        $delete = UserModel::where('nik', $request->id)->delete();
        if($delete){
            return back()->with('success', 'Data berhasil dihapus');
        }
    }

    public function exportadministrator(){
        $admin = UserModel::join('jabatan', 'jabatan.jabatan_id', '=', 'users.jabatan_id')->where('users.role', 'Admin')->get();
        $jabatan = JabatanModel::all();
        $data = [
            'karyawan' => $admin,
            'jabatan' => $jabatan
        ];
        return view('pages.admin.user.export')->with($data);
    }
    public function exportkaryawan(){
        $karyawan = UserModel::join('jabatan', 'jabatan.jabatan_id', '=', 'users.jabatan_id')->where('users.role', 'Karyawan')->get();
        $jabatan = JabatanModel::all();
        $data = [
            'karyawan' => $karyawan,
            'jabatan' => $jabatan
        ];
        return view('pages.admin.user.export')->with($data);
    }
    public function tolakcuti(Request $request){
        $update = DokumenCutiModel::where('dokumen_id', $request->dokumen_id)->update(['status' => 'Ditolak', 'alasan_ditolak' => $request->alasan_ditolak]);
        if($update){
            return back()->with('success', 'Data berhasil ditolak');
        }
    }
    public function setujuicuti(Request $request){
        $update = DokumenCutiModel::where('dokumen_id', $request->id)->update(['status' => 'Disetujui', 'tanggal_disetujui' => date('Y-m-d')]);
        if($update){
            return back()->with('success', 'Data berhasil disetujui');
        }
    }
    public function detailabsensi(Request $request){
        $split = explode('-', $request->tanggal);
        $jumlahhari = cal_days_in_month(CAL_GREGORIAN, $split[1], $split[0]);
        $karyawan = UserModel::where('nik', $request->user)->first();
        $absen = AbsensiModel::where('nik', $request->user)->whereBetween('tanggal_absen', [date($request->tanggal.'-01'), date($request->tanggal.'-'.$jumlahhari)])->get();
        $data = [
            'absen' => $absen,
            'tanggal' => $request->tanggal,
            'karyawan' => $karyawan
        ];
        return view('pages.admin.absensi.detail')->with($data);
    }
    public function exportabsensi(Request $request){
        $tanggal = $request->tanggal;
        $karyawan = UserModel::where('role', 'Karyawan')->get();
        $split = explode('-', $tanggal);
        $jumlahhari = cal_days_in_month(CAL_GREGORIAN, $split[1], $split[0]);

        $pdf = new Fpdf();
        $pdf->AddPage('L', array(297, 210));
        $pdf->SetMargins(25, 25, 25, 25);
        $pdf->SetFont('Times', 'B', 16);
        $pdf->Cell(0, 1, '', 0, 1, 'C');
        $pdf->Cell(0, 15, 'ABSENSI KARYAWAN PT. VILO GELATO', 1, 1, 'C');
        $pdf->SetFont('Times', '', 12);
        $pdf->Cell(0, 10, 'Periode : '.date('F Y', strtotime($tanggal)), 1, 1, 'C');
        
        $pdf->SetFont('Times', 'B', 12);
        $pdf->Cell(10, 10, 'No', 1, 0, 'C');
        $pdf->Cell(56, 10, 'Nama', 1, 0, 'C');
        $pdf->Cell(41, 10, 'NIK', 1, 0, 'C');
        $pdf->Cell(40, 10, 'Tepat Waktu', 1, 0, 'C');
        $pdf->Cell(30, 10, 'Izin', 1, 0, 'C');
        $pdf->Cell(30, 10, 'Terlambat', 1, 0, 'C');
        $pdf->Cell(40, 10, 'Tanpa Keterangan', 1, 1, 'C');
        
        $pdf->SetFont('Times', '', 12);            
        $no = 1;
        foreach ($karyawan as $key => $value) {
            $hadir = DB::table('absensis')->where('nik', $value->nik)->whereBetween('tanggal_absen', [date($tanggal.'-01'), date($tanggal.'-'.$jumlahhari)])->where('status_absen', 'Tepat Waktu')->get()->count();

            $izin = DB::table('absensis')->where('nik', $value->nik)->whereBetween('tanggal_absen', [date($tanggal.'-01'), date($tanggal.'-'.$jumlahhari)])->where('status_absen', 'Izin')->get()->count();

            $terlambat = DB::table('absensis')->where('nik', $value->nik)->whereBetween('tanggal_absen', [date($tanggal.'-01'), date($tanggal.'-'.$jumlahhari)])->where('status_absen', 'Terlambat')->get()->count();

            $pdf->Cell(10, 10, $no++, 1, 0, 'C');
            $pdf->Cell(56, 10, $value->nama, 1, 0, 'C');
            $pdf->Cell(41, 10, $value->nik, 1, 0, 'C');
            $pdf->Cell(40, 10, $hadir, 1, 0, 'C');
            $pdf->Cell(30, 10, $izin, 1, 0, 'C');
            $pdf->Cell(30, 10, $terlambat, 1, 0, 'C');
            $pdf->Cell(40, 10, $jumlahhari - ($hadir + $izin + $terlambat), 1, 1, 'C');
        }
        $pdf->Ln(15);
        $pdf->Output();
        exit;
    }
}
