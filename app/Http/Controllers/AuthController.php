<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\UserModel;
use DB, Session;

class AuthController extends Controller
{
    public function login(){
    	return view('auth.login');
    }
    public function process_login(Request $request){
    	$validated = $request->validate([
    		'nip' => 'required',
    		'password' => 'required'
    	]);
    	$user = new UserModel;
    	$finduser = $user->where('nik', $request->nip);
    	if($finduser->count() > 0){
    		$getuser = $finduser->first();
    		if(password_verify($request->password, $getuser->password)){
                $request->session()->put('nik', $getuser->nik);
                $request->session()->put('role', $getuser->role);
                if($getuser->role == 'Admin'){
                    return redirect(route('admin.dashboard'));
                }elseif($getuser->role == 'Karyawan'){
                    return redirect(route('user.dashboard'));
                }
    		} else {
    			return back()->with('error', 'NIP atau Password Salah');	
    		}
    	} else {
    		return back()->with('error', 'NIP atau Password Salah');
    	}
    }
    public function logout(){
        if( Session::has('role') && Session::has('nik') ){
            Session::pull('role');
            Session::pull('nik');
            return redirect('')->with('success', 'Berhasil logout');
        } else {
            return redirect(route('login'))->with('success', 'Berhasil logout');
        }
    }
}