<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\UserModel;
use App\Models\JabatanModel;
use App\Models\JamKerjaModel;
use App\Models\DokumenCutiModel;
use App\Models\AbsensiModel;
use Session;
use Codedge\Fpdf\Fpdf\Fpdf;

class UserController extends Controller
{
    public function dashboard(){
    	$administrator = UserModel::where('role', 'Admin')->count();
        $karyawan = UserModel::where('role', 'Karyawan')->count();
        $cuti = DokumenCutiModel::where('status', 'Sedang Diproses')->count();
        $absennow = AbsensiModel::where('nik', Session::get('nik'))->where('tanggal_absen', date('d-m-Y'))->first();
        $data = [
            'administrator' => $administrator,
            'karyawan' => $karyawan,
            'cuti' => $cuti,
            'absennow' => $absennow
        ];
    	return view('pages.karyawan.dashboard.index')->with($data);
    }
    public function absen(Request $request){
    	$cuti = DokumenCutiModel::where('nik', Session::get('nik'))->get();
        if(!$request->bulan && !$request->tahun){
            $absen = AbsensiModel::where('nik', Session::get('nik'))->where('tanggal_absen', '>=', date('Y-m-01'))->where('tanggal_absen', '<=', date('Y-m-d'))->get();
        } elseif($request->bulan && !$request->tahun){
            $absen = AbsensiModel::where('nik', Session::get('nik'))->where('tanggal_absen', '>=', date('Y-'.$request->bulan.'-01'))->where('tanggal_absen', '<=', date('Y-'.$request->bulan.'-d'))->get();
        } elseif(!$request->bulan && $request->tahun){
            $absen = AbsensiModel::where('nik', Session::get('nik'))->where('tanggal_absen', '>=', date($request->tahun.'-m-01'))->where('tanggal_absen', '<=', date($request->tahun.'-m-d'))->get();
        } elseif($request->bulan && $request->tahun){
            $absen = AbsensiModel::where('nik', Session::get('nik'))->where('tanggal_absen', '>=', date($request->tahun.'-'.$request->bulan.'-01'))->where('tanggal_absen', '<=', date($request->tahun.'-'.$request->bulan.'-d'))->get();
        }
    	$user = UserModel::where('nik', Session::get('nik'))->first();
    	$data = [
    		'cuti' => $cuti,
    		'user' => $user,
            'absen' => $absen
    	];
    	return view('pages.karyawan.absen.index')->with($data);
    }
    public function cuti(){
    	$cuti = DokumenCutiModel::where('nik', Session::get('nik'))->get();
    	$user = UserModel::where('nik', Session::get('nik'))->first();
    	$data = [
    		'cuti' => $cuti,
    		'user' => $user
    	];
    	return view('pages.karyawan.cuti.index')->with($data);
    }
    public function storeAbsen(Request $request){
        date_default_timezone_set('Asia/Jakarta');

        $cekabsensi = AbsensiModel::where('nik', Session::get('nik'))->where('tanggal_absen', date('Y-m-d'))->first();
        $getjam = JamKerjaModel::where('jam_id', $request->jam_id)->first();
        if($cekabsensi){
            if(isset($_POST['hadir'])){
                if($cekabsensi->jam_masuk != null){
                    $up = AbsensiModel::where('nik', Session::get('nik'))->where('tanggal_absen', date('Y-m-d'))->update(['jam_pulang' => date('H:i:s')]);
                    if($up){
                        return back()->with('success', 'Berhasil Absen Jam Pulang');
                    }
                } else {
                    return back();
                }
            } else {
                return back();
            }
        } else {
            $abs = new AbsensiModel;
            if(isset($_POST['hadir'])){
                if( (strtotime(date('H:i:s')) - strtotime($getjam->jam_masuk)) > 900){
                    $abs->status_absen = 'Terlambat';
                } else {
                    $abs->status_absen = 'Tepat Waktu';
                }
                $abs->nik = Session::get('nik');
                $abs->jam_masuk = date('H:i:s');
                $abs->jam_id = $request->jam_id;
                $abs->tanggal_absen = date('Y-m-d');
                $abs->save();
                if($abs){
                    return back()->with('success', 'Berhasil Absen Jam Masuk');
                }
            } elseif(isset($_POST['izin'])){
                if(!$request->file_izin){
                    return back()->with('error','Isi file izin terlebih dahulu');
                } else {
                    $tujuan_upload = 'assets/images/izin/';
                    $file = $request->file('file_izin');
                    $namafile = time().'_'.$file->getClientOriginalName();
                    if($file->move($tujuan_upload,$namafile)){
                        $abs->nik = Session::get('nik');
                        $abs->status_absen = 'Izin';
                        $abs->tanggal_absen = date('Y-m-d');
                        $abs->file_izin = $namafile;
                        $abs->save();

                        $abs->save();

                        if($abs){
                            return back()->with('success', 'Berhasil absen');
                        }
                    }
                }
            }
        }
    }
    public function storeCuti(Request $request){
        $cuti = new DokumenCutiModel;
        $cuti->nik = $request->nik;
        $cuti->tanggal_mulai_cuti = $request->tanggal_mulai_cuti;
        $cuti->tanggal_berakhir_cuti = $request->tanggal_berakhir_cuti;
        $cuti->alasan_cuti = $request->alasan_cuti;
        $cuti->status = 'Sedang Diproses';
        $cuti->save();
        if($cuti){
            return back()->with('success', 'Data cuti berhasil dikirim');
        }
    }
    public function updateCuti (Request $request){
        $data = [
            'tanggal_mulai_cuti' => $request->tanggal_mulai_cuti,
            'tanggal_berakhir_cuti' => $request->tanggal_berakhir_cuti,
            'alasan_cuti' => $request->alasan_cuti,
        ];
        $update = DokumenCutiModel::where('dokumen_id', $request->id)->update($data);
        if($update){
            return back()->with('success', 'Data berhasil diedit');
        }
    }

    public function showDokumen(Request $request){
        $id = $request->id;
        $data = DokumenCutiModel::where('dokumen_id', $id)->first();
        return response()->json(['data' => $data]);
    }

    public function deletecuti (Request $request){
        $delete = DokumenCutiModel::where('dokumen_id', $request->id)->delete();
        if($delete){
            return back()->with('success', 'Data berhasil dihapus');
        }
    }

    public function profil (Request $request){
        $karyawan = UserModel::join('jabatan', 'jabatan.jabatan_id', '=', 'users.jabatan_id')->where('users.nik', Session::get('nik'))->first();
        $jabatan = JabatanModel::all();
        $data = [
            'karyawan' => $karyawan,
            'jabatan' => $jabatan
        ];
        return view('pages.karyawan.profil.index')->with($data);
    }
    public function updateprofil (Request $request){
        $cekkaryawan = UserModel::where('nik', $request->niklama)->first();
        if($request->nik != $request->niklama){
            $cekkaryawan2 = UserModel::where('nik', $request->nik)->get()->count();
            if($cekkaryawan2 > 0){
                return back()->with('error', 'NIK sudah digunakan');
            }
        }
        if($request->password == $cekkaryawan->password){
            $pass = $request->password;
        } else {
            $pass = password_hash($request->password, PASSWORD_DEFAULT);
        }
        $request->session()->put('nik', $request->nik);
        $data = [
            'nama' => $request->nama,
            'nik' => $request->nik,
            'password' => $pass,
            'jenis_kelamin' => $request->jenis_kelamin,
            'no_hp' => $request->no_hp,
            'alamat' => $request->alamat,
        ];
        $update = UserModel::where('nik', $request->niklama)->update($data);
        if($update){
            return back()->with('success', 'Data berhasil diupdate');
        }
    }
    public function filePdfCuti(Request $request){
        date_default_timezone_set('Asia/Jakarta');
        $cuti = DokumenCutiModel::where('dokumen_id', $request->id)->first();
        $countstrtotime = (strtotime($cuti->tanggal_berakhir_cuti) - strtotime($cuti->tanggal_mulai_cuti))/(3600*24);
        $karyawan = UserModel::join('jabatan', 'jabatan.jabatan_id', '=', 'users.jabatan_id')->where('users.nik', $cuti->nik)->first();

        $pdf = new Fpdf();
        $pdf->AddPage('P', array(210, 297));
        $pdf->SetMargins(25, 25, 25, 25);
        $pdf->SetFont('Times', 'B', 14);
        $pdf->Cell(0, 10, '', 0, 1, 'C');
        $pdf->Cell(0, 10, 'FORMULIR CUTI / IZIN KARYAWAN', 0, 0, 'C'); 
        $pdf->Ln(15);

        $pdf->SetFont('Times', '', 12);
        $pdf->Cell(80, 10, 'Perihal : Izin '.$cuti->alasan_cuti, 0, 0, 'L');
        $pdf->Cell(80, 10, 'Jakarta, '.date('d F Y', strtotime($cuti->tanggal_disetujui)), 0, 1, 'R');
        $pdf->Ln(5);
        
        $pdf->Cell(0, 10, 'Kepada Yth,', 0, 0, 'L'); 
        $pdf->Ln(7);
        $pdf->Cell(0, 10, 'Pimpinan PT. Vilo Gelato', 0, 0, 'L'); 
        $pdf->Ln(7);
        $pdf->Cell(0, 10, 'Di tempat', 0, 1, 'L'); 
        $pdf->Ln(5);

        $pdf->Cell(0, 10, 'Dengan hormat,', 0, 0, 'L'); 
        $pdf->Ln(7);
        $pdf->Cell(0, 10, 'Saya yang bertanda tangan dibawah ini :', 0, 1, 'L');
        $pdf->Ln(3);
        $pdf->Cell(15, 10, '', 0, 0, 'L');
        $pdf->Cell(25, 10, 'Nama', 0, 0, 'L'); 
        $pdf->Cell(5, 10, ':', 0, 0, 'L'); 
        $pdf->Cell(0, 10, $karyawan->nama, 0, 0, 'L'); 
        $pdf->Ln(7);
        $pdf->Cell(15, 10, '', 0, 0, 'L');
        $pdf->Cell(25, 10, 'Alamat', 0, 0, 'L'); 
        $pdf->Cell(5, 10, ':', 0, 0, 'L'); 
        $pdf->Cell(0, 10, $karyawan->alamat, 0, 0, 'L'); 
        $pdf->Ln(7);
        $pdf->Cell(15, 10, '', 0, 0, 'L');
        $pdf->Cell(25, 10, 'No. Telp', 0, 0, 'L'); 
        $pdf->Cell(5, 10, ':', 0, 0, 'L'); 
        $pdf->Cell(0, 10, $karyawan->no_hp, 0, 0, 'L'); 
        $pdf->Ln(7);
        $pdf->Cell(15, 10, '', 0, 0, 'L');
        $pdf->Cell(25, 10, 'Jabatan', 0, 0, 'L'); 
        $pdf->Cell(5, 10, ':', 0, 0, 'L'); 
        $pdf->Cell(0, 10, $karyawan->nama_jabatan, 0, 1, 'L'); 
        $pdf->Ln(5);
        $pdf->MultiCell(160, 7, 'Bermaksud untuk mengajukan permohonan izin "'.$cuti->alasan_cuti.'" selama '.$countstrtotime.' hari terhitung dari '.date('d F Y', strtotime($cuti->tanggal_mulai_cuti)).' sampai dengan '.date('d F Y', strtotime($cuti->tanggal_berakhir_cuti)).'.', 0, 'J');
        $pdf->Ln(2);
        $pdf->MultiCell(160, 7, 'Demikian surat permohonan cuti ini saya ajukan, untuk menjadi pertimbangan bagi Bapak/Ibu pimpinan', 0, 'J');
        $pdf->Ln(2);
        $pdf->Cell(0, 10, 'Atas perhatiannya, saya ucapkan terimakasih', 0, 1, 'J');
        $pdf->Ln(7);
        $pdf->Cell(80, 10, 'Hormat Saya,', 0, 0, 'C');
        $pdf->Cell(80, 10, 'Menyetujui,', 0, 1, 'C');
        $pdf->Ln(5);
        $pdf->SetFont('Symbol', 'B', 20);
        $pdf->Cell(80, 10, $karyawan->nama, 0, 1, 'C');
        $pdf->Ln(5);
        $pdf->SetFont('Times', '', 12);
        $imagepath = base_path('assets/images/qrcode.png');
        $pdf->Image($imagepath, 135, 195, 20, 0);
        $pdf->Cell(80, 10, $karyawan->nama, 0, 0, 'C');
        $pdf->Cell(80, 10, 'Pimpinan PT. Vilo Gelato Menteng', 0, 1, 'C');
        $pdf->Ln(7);
        $pdf->Output();
        exit;
    }
}
