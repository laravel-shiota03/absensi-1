<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\AuthController;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\UserController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::controller(AuthController::class)->group(function(){
	Route::get('/', 'login')->name('login');
	Route::post('/process', 'process_login')->name('process.login');
	Route::get('/logout', 'logout')->name('logout');
});

Route::controller(AdminController::class)->group(function(){
	Route::prefix('/admin')->group(function(){
		Route::get('/', 'dashboard')->name('admin.dashboard');
		Route::prefix('/data-administrator')->group(function(){
			Route::get('/', 'administrator')->name('admin.administrator');
			Route::get('/export', 'exportadministrator')->name('admin.exportadministrator');
		});
		Route::prefix('/data-karyawan')->group(function(){
			Route::get('/', 'karyawan')->name('admin.karyawan');
			Route::post('/store', 'storeUser')->name('admin.user.store');
			Route::get('/export', 'exportkaryawan')->name('admin.exportkaryawan');
		});
		Route::prefix('/absensi')->group(function(){
			Route::get('/', 'absensi')->name('admin.absensi');
			Route::get('/view/{tanggal}', 'viewabsensi');
			Route::get('/detail', 'detailabsensi');
			Route::get('/export/{tanggal}', 'exportabsensi');
		});
		Route::prefix('/user')->group(function(){
			Route::get('/', 'user')->name('admin.user');
			Route::get('/view/{id}', 'viewuser');
			Route::get('/edit/{id}', 'showuser');
			Route::post('/update', 'updateuser')->name('admin.updateuser');
			Route::get('/delete/{id}', 'deleteuser');
		});
		Route::prefix('/cuti')->group(function(){
			Route::get('/', 'cuti')->name('admin.cuti');
			Route::post('/tolak', 'tolakcuti')->name('admin.tolakcuti');
			Route::get('/setujui/{id}', 'setujuicuti');
		});
		Route::prefix('/profil')->group(function(){
			Route::get('/', 'profil')->name('admin.profil');
		});
		Route::prefix('/jabatan')->group(function(){
			Route::get('/', 'jabatan')->name('admin.jabatan');
			Route::get('/show', 'showJabatan')->name('admin.jabatan.show');
			Route::post('/store', 'storeJabatan')->name('admin.jabatan.store');
			Route::get('/delete/{id}', 'deleteJabatan');
		});
		Route::prefix('/jam-kerja')->group(function(){
			Route::get('/', 'jamKerja')->name('admin.jam.kerja');
			Route::post('/update', 'updateJamKerja')->name('admin.jam.kerja.update');
		});
	});
});
Route::controller(UserController::class)->group(function(){
	Route::prefix('/user')->group(function(){
		Route::get('/', 'dashboard')->name('user.dashboard');
		Route::prefix('/data-cuti')->group(function(){
			Route::get('/', 'cuti')->name('user.cuti');
			Route::post('/store', 'storeCuti')->name('user.cuti.store');
			Route::get('/delete/{id}', 'deletecuti');
			Route::get('/show', 'showDokumen')->name('user.dokumen.show');
			Route::post('/update', 'updateCuti')->name('user.cuti.update');
			Route::get('/file-pdf/{id}', 'filePdfCuti');
		});
		Route::prefix('/data-absen')->group(function(){
			Route::get('/', 'absen')->name('user.absen');
			Route::post('/store', 'storeAbsen')->name('user.absen.store');
		});
		Route::prefix('/profil')->group(function(){
			Route::get('/', 'profil')->name('user.profil');
			Route::post('/update', 'updateprofil')->name('user.profil.update');
		});
	});
});