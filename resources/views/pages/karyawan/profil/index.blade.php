@extends('pages.karyawan.layout')
@section('pageheader', 'Profil')
@section('content')
<script type="text/javascript">
  document.getElementsByClassName('menu-side')[3].classList.add('active')
</script>
<hr class="my-1">
<form class="alert alert-primary mt-lg-5 mt-3 mt-2" action="{{ route('user.profil.update') }}" method="post">
	@csrf
	<h1><em>Hello, {{ $karyawan->nama }}</em></h1>
	<hr>
	<div class="row">
		<input type="hidden" name="niklama" readonly value="{{ $karyawan->nik }}" class="form-control">
		<div class="col-lg-4 col-md-6 col-12">
			<div class="form-group">
				<label for="nip">NIK</label>
				<input type="number" name="nik" value="{{ $karyawan->nik }}" class="form-control" required="">
				@error('nip')<small class="text-danger"><em>{{ $message }}</em></small>@enderror
			</div>
		</div>
		<div class="col-lg-4 col-md-6 col-12">
			<div class="form-group">
				<label for="nama">Nama</label>
				<input type="text" required name="nama" value="{{ $karyawan->nama }}" class="form-control">
				@error('nama')<small class="text-danger"><em>{{ $message }}</em></small>@enderror
			</div>
		</div>
		<div class="col-lg-4 col-md-6 col-12">
			<div class="form-group">
				<label for="jenis_kelamin" >Jenis Kelamin</label>
				<select class="form-control" name="jenis_kelamin">
					<option value="Laki - Laki" @if($karyawan->jenis_kelamin == 'Laki - Laki'){{ "selected"; }}@endif >Laki - Laki</option>
					<option value="Perempuan" @if($karyawan->jenis_kelamin == 'Perempuan'){{ "selected"; }}@endif >Perempuan</option>
				</select>
			</div>
		</div>
		<div class="col-lg-4 col-md-6 col-12">
			<div class="form-group">
				<label for="id_jabatan">Jabatan</label>
				<input type="text" name="" value="{{ $karyawan->nama_jabatan }}" readonly="" class="form-control">
			</div>
		</div>
		<div class="col-lg-4 col-md-6 col-12">
			<div class="form-group">
				<label for="no_hp">Nomor Hp</label>
				<input type="number" required name="no_hp" value="{{ $karyawan->no_hp }}" class="form-control">
				@error('no_hp')<small class="text-danger"><em>{{ $message }}</em></small>@enderror
			</div>
		</div>
		<div class="col-lg-4 col-md-6 col-12">
			<div class="form-group">
				<label for="alamat">Alamat</label>
				<input type="text" required name="alamat" value="{{ $karyawan->alamat }}" class="form-control">
				@error('alamat')<small class="text-danger"><em>{{ $message }}</em></small>@enderror
			</div>
		</div>
		<div class="col-lg-4 col-md-6 col-12">
			<div class="form-group">
				<label for="password">Password</label>
				<input required="" type="password" name="password" value="{{ $karyawan->password }}" class="form-control">
				@error('password')<small class="text-danger"><em>{{ $message }}</em></small>@enderror
			</div>
		</div>
		<div class="col-lg-4 col-12">
			<div class="form-group">
				<label></label>
				<button class="btn btn-info w-100 btn-outline-light">Update</button>
			</div>
		</div>
	</div>
</form>
@endsection