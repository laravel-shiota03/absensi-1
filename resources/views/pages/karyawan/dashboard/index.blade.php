@extends('pages.karyawan.layout')
@section('pageheader', 'Dashboard')
@section('content')
<script type="text/javascript">
  document.getElementsByClassName('menu-side')[0].classList.add('active')
</script>
<style type="text/css">
  .icon-dashboard i{
    transform: rotate(15deg);
    color: whitesmoke;
    opacity: 50%;
    font-size: 4rem
  }
</style>
<div class="container">
  <div class="row">
    <div class="col-lg-3 col-md-6 col-12 my-2">
      <div class="bg-primary rounded text-white p-2">
        <div class="d-flex align-items-center">
          <div class="col-9 col-lg-8">
            <h4 class="text-white">Total Hadir</h4>
            <h1 class="text-white">{{ $administrator }}</h1>
          </div>
          <h1 class="col-3 col-lg-4 text-end icon-dashboard"><i class="fas fa-thumbs-up"></i></h1>
        </div>
        <hr class="my-1">
        <a href="{{ route('admin.administrator') }}" class="text-white text-decoration-none">See more <i class="fas fa-arrow-right ms-2"></i></a>
      </div>
    </div>
    <div class="col-lg-3 col-md-6 col-12 my-2">
      <div class="bg-info rounded text-white p-2">
        <div class="d-flex align-items-center">
          <div class="col-9 col-lg-8">
            <h4 class="text-white">Total Izin</h4>
            <h1 class="text-white">{{ $karyawan }}</h1>
          </div>
          <h1 class="col-3 col-lg-4 text-end icon-dashboard"><i class="fas fa-envelope"></i></h1>
        </div>
        <hr class="my-1">
        <a href="{{ route('admin.karyawan') }}" class="text-white text-decoration-none">See more <i class="fas fa-arrow-right ms-2"></i></a>
      </div>
    </div>
    <div class="col-lg-3 col-6 col-12 my-2">
      <div class="bg-danger rounded text-white p-2">
        <div class="d-flex align-items-center">
          <div class="col-9 col-lg-8">
            <h4 class="text-white">Total Alpa</h4>
            <h1 class="text-white">{{ $cuti }}</h1>
          </div>
          <h1 class="col-3 col-lg-4 text-end icon-dashboard"><i class="fas fa-circle-xmark"></i></h1>
        </div>
        <hr class="my-1">
        <a href="{{ route('admin.cuti') }}" class="text-white text-decoration-none">See more <i class="fas fa-arrow-right ms-2"></i></a>
      </div>
    </div>
    <div class="col-lg-3 col-6 col-12 my-2">
      <div class="bg-warning rounded text-white p-2">
        <div class="d-flex align-items-center">
          <div class="col-9 col-lg-8">
            <h4 class="text-white">Terlambat</h4>
            <h1 class="text-white">{{ $cuti }}</h1>
          </div>
          <h1 class="col-3 col-lg-4 text-end icon-dashboard"><i class="fas fa-thumbs-down"></i></h1>
        </div>
        <hr class="my-1">
        <a href="{{ route('admin.cuti') }}" class="text-white text-decoration-none">See more <i class="fas fa-arrow-right ms-2"></i></a>
      </div>
    </div>
  </div>
  <hr class="my-3">
  <div class="table-responsive py-2 px-md-4 px-2 bg-white rounded border">
    <h3><strong>Data absen hari ini</strong></h3>
    <table class="table table-striped table-hovered table-bordered">
      <thead>
        <tr>
          <th>Tanggal</th>
          <th>Shift Kerja</th>
          <th>Jam Masuk</th>
          <th>Jam Pulang</th>
          <th>Status</th>
          <th>File Izin</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td class="text-center"><?php echo date('d/m/Y'); ?></td>
          @if($absennow)
          <td class="text-center">Shift Kerja</td>
          <td class="text-center">{{ $absennow->jam_masuk }}</td>
          <td class="text-center">{{ $absennow->jam_pulang }}</td>
          <td class="text-center">{{ $absennow->status_absen }}</td>
          <td class="text-center">
            <a href=""><i class="fas fa-file-pdf btn btn-warning"></i></a>
          </td>
          @else
          <td class="text-center" colspan="5">
            <em>Belum absen hari ini</em>
          </td>
          @endif
        </tr>
      </tbody>
    </table>
    <a href="{{ route('user.absen') }}" class="text-primary mt-2">Lihat Selengkapnya ...</a>
  </div>
@endsection