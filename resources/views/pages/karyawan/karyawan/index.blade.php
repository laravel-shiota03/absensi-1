@extends('pages.admin.layout')
@section('pageheader', 'Data Karyawan')
@section('content')
<script type="text/javascript">
  document.getElementsByClassName('menu-side')[1].classList.add('active')
  document.getElementsByClassName('menu-dropdown')[1].classList.add('bg-secondary')
</script>
<div class="container">
	<div class="row">
		<h4 class="col-lg-6 col-12 my-2"><i class="fas fa-user me-3"></i>Data Karyawan</h4>
		<div class="col-lg-6 col-12 my-2 text-lg-end">
			<div>
				<button class="btn btn-success my-1 my-lg-0" data-bs-toggle="modal" data-bs-target="#addUser"><i class="fas fa-plus-square me-2"></i> Tambah Data</button>
				<a target="__blank" href="" class="btn btn-info text-white my-1 my-lg-0"><i class="fas fa-file-excel me-2"></i>Export</a>
			</div>
		</div>
	</div>
	<br>
	<div class="table-responsive">
		<table class="table table-hovered table-bordered table-striped" id="datatable">
			<thead>
				<tr>
					<th>No</th>
					<th>Nama</th>
					<th>NIP</th>
					<th>Jabatan</th>
					<th>Aksi</th>
				</tr>
			</thead>
			<tbody>
				<?php $no = 1; ?>
				@foreach($karyawan as $kar)
					<tr>
						<td>{{ $no++ }}</td>
						<td>{{ $kar->nama }}</td>
						<td>{{ $kar->nip }}</td>
						<td>{{ $kar->nama_jabatan }}</td>
						<td>
							<button id="" class="view my-1 btn btn-primary"><i class="fas fa-eye"></i></button>
							<button id="" class="edit my-1 btn btn-warning"><i class="fas fa-edit text-white"></i></button>
							<a href="" class="my-1 btn btn-danger"><i class="fas fa-trash"></i></a>
						</td>
					</tr>
				@endforeach
			</tbody>
		</table>
	</div>
</div>
<div class="modal fade" id="addUser" tabindex="-1" aria-labelledby="addUserLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
		  	<div class="modal-header">
			    <h5 class="modal-title" id="addUserLabel">Tambah Data Karyawan</h5>
			    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
			</div>
			<div class="modal-body">
			  	<form action="{{ route('admin.user.store') }}" method="post">
			  		@csrf
			  		<div class="form-group">
				  		<label>Nama</label>
				        <input type="text" name="nama" required="" class="form-control">
				    </div>
				    <div class="form-group mt-3">
				    	<label>Nomor Induk Pegawai</label>
				    	<input type="number" name="nip" required="" class="form-control">
				    </div>
				    <div class="form-group mt-3">
				    	<label>Password</label>
				    	<input type="password" name="password" required="" class="form-control">
				    </div>
				    <div class="form-group mt-3">
				    	<label>Jabatan</label>
				    	<select name="id_jabatan" class="form-control">
				    		@foreach($jabatan as $jab)
				    			<option value="{{ $jab->jabatan_id }}">{{ $jab->nama_jabatan }}</option>
				    		@endforeach
				    	</select>
				    </div>
				    <div class="form-group mt-3">
				    	<label>Jenis Kelamin</label>
				    	<select class="form-control" name="jenis_kelamin">
				    		<option value="Pria">Pria</option>
				    		<option value="Wanita">Wanita</option>
				    	</select>
				    </div>
			        <br>
			        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Keluar</button>
			        <button type="submit" name="addkaryawan" class="btn btn-primary">Simpan</button>
			    </form>
		  	</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	$('#datatable').dataTable();
</script>
@endsection