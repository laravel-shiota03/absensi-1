@extends('pages.karyawan.layout')
@section('pageheader', 'Data Absensi')
@section('content')
<script type="text/javascript">
  document.getElementsByClassName('menu-side')[1].classList.add('active')
</script>
<div class="container">
	<div class="row">
		<h4 class="col-lg-6 col-12 my-2"><i class="fas fa-database me-3"></i>Data Absensi</h4>
	</div>
	<br>
	<form class="p-md-4 p-2 border rounded bg-white" method="post" action="{{ route('user.absen.store') }}" enctype="multipart/form-data">
		@csrf
		<div class="row">
			<div class="col-6">
				<label>Shift</label>
				<select class="form-control" name="jam_id">
					<option value="1">Pagi</option>
					<option value="2">Siang</option>
				</select>
				<label class="mt-2">File Izin (Jika Ingin Izin)</label>
				<input type="file" name="file_izin" class="form-control" accept="application/pdf, application/doc, application/docx"><br>
			</div>
			<div class="col-6">
				<div class="waktu alert alert-warning mb-3"></div>
				<button name="hadir" class="btn btn-primary w-100">Hadir</button>
				<button name="izin" class="btn btn-warning w-100">Kirim File Izin</button>
			</div>
		</div>
	</form>
	<br>
	<div class="bg-white p-md-4 p-2 rounded border">
		<form method="get" style="margin-bottom: -24px">
			<h4>Filter</h4>
			<div class="row">
				<div class="col-lg-3 col-md-4 col-12 my-1">
					<div class="form-group ">
						<select name="bulan" required class="form-control">
							<option disabled selected>Bulan</option>
							<option value="01">Januari</option>
							<option value="02">Februari</option>
							<option value="03">Maret</option>
							<option value="04">April</option>
							<option value="05">Mei</option>
							<option value="06">Juni</option>
							<option value="07">Juli</option>
							<option value="08">Agustus</option>
							<option value="09">September</option>
							<option value="10">Oktober</option>
							<option value="11">November</option>
							<option value="12">Desember</option>
						</select>
					</div>
				</div>
				<div class="col-lg-3 col-md-4 col-12 my-1">
					<div class="form-group ">
						<select name="tahun" required class="form-control">
							<option disabled selected>Tahun</option>
							<?php for ($i=2010; $i < 2030; $i++) : ?>
								<option value="{{ $i }}">{{ $i }}</option>
							<?php endfor; ?>
						</select>
					</div>
				</div>
				<div class="col-md-3 col-12 my-1">
					<div class="form-group ">
						<button class="btn btn-primary form-control"><i class="fas fa-filter me-2"></i>Filter</button>
					</div>
				</div>
			</div>
		</form>
		<hr class="mb-3"></hr>
		<div class="table-responsive">
			<table class="border w-100 table table-striped table-bordered table-hovered" id="datatable">
				<thead>
			        <tr>
						<th>Tanggal</th>
						<th>Shift Kerja</th>
						<th>Jam Masuk</th>
						<th>Jam Pulang</th>
						<th>Status</th>
			        </tr>
		     	</thead>
				<tbody>
					@foreach($absen as $ab)
					<tr>
						<td>{{ $ab->tanggal_absen }}</td>
						<td>@if($ab->jam_id == 1) Pagi @elseif($ab->jam_id == 2) Siang @endif</td>
						<td>{{ $ab->jam_masuk }}</td>
						<td>{{ $ab->jam_pulang }}</td>
						<td>{{ $ab->status_absen }}</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
</div>
<script type="text/javascript">
	$('#datatable').dataTable();
	window.setTimeout("waktu()", 1000);
		function waktu() {
			var waktu = new Date();
			setTimeout("waktu()", 1000);
			var hari = ['Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', "Jum'at", 'Sabtu'];
			var bulan = ['Januari', 'Februari', 'Maret', 'April', 'Mei', "Juni", 'Juli', 'Agustus', 'September', 'OKtober', 'November', 'Desember'];
			if(waktu.getSeconds() < 10){ var detik = '0'+waktu.getSeconds() } else { var detik = waktu.getSeconds() }
			if(waktu.getMinutes() < 10){ var menit = '0'+waktu.getMinutes() } else { var menit = waktu.getMinutes() }
			if(waktu.getHours() < 10){ var jam = '0'+waktu.getHours() } else { var jam = waktu.getHours() }
			if(waktu.getDate() < 10){ var tanggal = '0'+waktu.getDate() } else { var tanggal = waktu.getDate() }

			$(".waktu").html('<i class="fas fa-clock me-md-3 me-2"></i>'+hari[waktu.getDay()]+', '+tanggal+' '+bulan[waktu.getMonth()]+' '+waktu.getFullYear()+' - '+jam+' : '+menit+' : '+detik);
		}
</script>
@endsection