@extends('pages.admin.layout')
@section('pageheader', 'Data Absensi Karyawan')
@section('content')
<script type="text/javascript">
  document.getElementsByClassName('menu-side')[2].classList.add('active')
</script>
<div class="container">
	<a target="__blank" href="" class="btn btn-success text-white my-1 my-lg-0"><i class="fas fa-arrow-left me-2"></i>Kembali</a>
	<br>
	<div class="row">
		<h4 class="col-lg-6 col-12 my-2"><i class="fas fa-database me-3"></i>Data Absensi</h4>
		<div class="col-lg-6 col-12 my-2 text-lg-end">
			<div>
				<a target="__blank" href="" class="btn btn-info text-white my-1 my-lg-0"><i class="fas fa-file-excel me-2"></i>Export</a>
			</div>
		</div>
	</div>
	<br>
	<div class="table-responsive">
		<table class="table table-hovered table-bordered table-striped" id="datatable">
			<thead>
				<tr>
					<th>No</th>
					<th>Nama</th>
					<th>NIP</th>
					<th>Kehadiran</th>
					<th>Izin</th>
					<th>Tanpa Keterangan</th>
					<th>Aksi</th>
				</tr>
			</thead>
			<tbody>
					<tr>
						<td>1</td>
						<td>User 1</td>
						<td>123456</td>
						<td class="text-center">20</td>
						<td class="text-center">5</td>
						<td class="text-center">5</td>
						<td>
							<a href="" class="my-1 btn btn-primary">Lihat Detail</a>
						</td>
					</tr>
			</tbody>
		</table>
	</div>
</div>
@endsection