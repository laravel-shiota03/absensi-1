@extends('pages.karyawan.layout')
@section('pageheader', 'Data Pengajuan Cuti Karyawan')
@section('content')
<script type="text/javascript">
  document.getElementsByClassName('menu-side')[2].classList.add('active')
</script>
<div class="container">
	<div class="row">
		<h4 class="col-lg-6 col-12 my-2"><i class="fas fa-file me-3"></i>Data Pengajuan Cuti Karyawan</h4>
		<div class="col-lg-6 col-12 my-2 text-lg-end">
			<div>
				<a data-bs-toggle="modal" data-bs-target="#ajukanCuti" class="btn btn-success text-white my-1 my-lg-0"><i class="fas fa-plus-square me-2"></i>Ajukan Cuti</a>
			</div>
		</div>
	</div>
	<br>
	<div class="table-responsive">
		<table class="table table-hovered table-bordered table-striped" id="datatable">
			<thead>
				<tr>
					<th>No</th>
					<th>Tanggal Pengajuan</th>
					<th>Alasan Cuti</th>
					<th>Status Pengajuan</th>
					<th>Alasan Ditolak</th>
					<th>File Cuti</th>
					<th>Aksi</th>
				</tr>
			</thead>
			<tbody>
				<?php $no = 1; ?>
				@foreach($cuti as $izin)
					<tr>
						<td>{{ $no++ }}</td>
						<td>{{ date('d/m/y', strtotime($izin->tanggal_mulai_cuti)) }} - {{ date('d/m/y', strtotime($izin->tanggal_berakhir_cuti)) }}</td>
						<td>{{ $izin->alasan_cuti }}</td>
						@if( $izin->status == 'Sedang Diproses' )
							<td>
								<span class="bg-warning py-1 px-2 rounded text-white">
									<em>{{ $izin->status }}</em>
								</span>
							</td>
							<td></td>
							<td></td>
							<td>
								<button id="{{ $izin->dokumen_id }}" class="view my-1 btn btn-warning"><i class="fas fa-edit"></i></button>
								<a href="{{ route('user.cuti') }}/delete/{{ $izin->dokumen_id }}" class="my-1 btn btn-danger"><i class="fas fa-trash"></i></a>
							</td>
						@elseif( $izin->status == 'Disetujui' )
							<td>
								<span class="bg-success py-1 px-2 rounded text-white">
									<em>{{ $izin->status }}</em>
								</span>
							</td>
							<td></td>
							<td>
								<a href="{{ route('user.cuti') }}/file-pdf/{{ $izin->dokumen_id }}" target="__blank" class="btn btn-info">
									<i class="fas fa-file-pdf"></i>
								</a>
							</td>
							<td>
								<a href="{{ route('user.cuti') }}/delete/{{ $izin->dokumen_id }}" class="my-1 btn btn-danger"><i class="fas fa-trash"></i></a>
							</td>
						@elseif( $izin->status == 'Ditolak' )
							<td>
								<span class="bg-danger py-1 px-2 rounded text-white">
									<em>{{ $izin->status }}</em>
								</span>
							</td>
							<td>{{ $izin->file }}</td>
							<td></td>
							<td>
								<a href="{{ route('user.cuti') }}/delete/{{ $izin->dokumen_id }}" class="my-1 btn btn-danger"><i class="fas fa-trash"></i></a>
							</td>
						@endif
					</tr>
				@endforeach	
			</tbody>
		</table>
	</div>
</div>
<div class="modal fade" id="ajukanCuti" tabindex="-1" aria-labelledby="ajukanCutiLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
		  	<div class="modal-header">
			    <h5 class="modal-title" id="ajukanCutiLabel">Ajukan Cuti</h5>
			    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
			</div>
			<div class="modal-body">
			  	<form action="{{ route('user.cuti.store') }}" method="post">
			  		@csrf
			  		<div class="form-group">
				  		<label>Nama</label>
				        <input type="text" name="nama" readonly="" value="{{ $user->nama }}" class="form-control">
				    </div>
				    <div class="form-group mt-3">
				    	<label>NIK</label>
				    	<input type="number" name="nik" readonly="" value="{{ $user->nik }}"class="form-control">
				    </div>
				    <div class="form-group mt-3">
				    	<label>Tanggal Mulai Cuti</label>
				    	<input type="date" name="tanggal_mulai_cuti" required="" class="form-control">
				    </div>
				    <div class="form-group mt-3">
				    	<label>Tanggal Berakhir Cuti</label>
				    	<input type="date" name="tanggal_berakhir_cuti" required="" class="form-control">
				    </div>
				    <div class="form-group mt-3">
				    	<label>Alasan Cuti</label>
				    	<textarea class="form-control" name="alasan_cuti" required=""></textarea>
				    </div>
			        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Keluar</button>
			        <button type="submit" class="btn btn-primary">Simpan</button>
			    </form>
		  	</div>
		</div>
	</div>
</div>
<div class="modal fade" id="dokumenajukanCuti" tabindex="-1" aria-labelledby="dokumenajukanCutiLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
		  	<div class="modal-header">
			    <h5 class="modal-title" id="dokumenajukanCutiLabel">Ajukan Cuti</h5>
			    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
			</div>
			<div class="modal-body">
			  	<form action="{{ route('user.cuti.update') }}" method="post">
			  		@csrf
			  		<div class="form-group">
				  		<label>Nama</label>
				  		<input type="hidden" name="id" readonly="" value="{{ $user->dokumen_id }}" class="form-control">
				        <input type="text" name="nama" readonly="" value="{{ $user->nama }}" class="form-control">
				    </div>
				    <div class="form-group mt-3">
				    	<label>NIK</label>
				    	<input type="number" name="nik" readonly="" value="{{ $user->nik }}"class="form-control">
				    </div>
				    <div class="form-group mt-3">
				    	<label>Tanggal Mulai Cuti</label>
				    	<input type="date" name="tanggal_mulai_cuti" required="" class="form-control">
				    </div>
				    <div class="form-group mt-3">
				    	<label>Tanggal Berakhir Cuti</label>
				    	<input type="date" name="tanggal_berakhir_cuti" required="" class="form-control">
				    </div>
				    <div class="form-group mt-3">
				    	<label>Alasan Cuti</label>
				    	<textarea class="form-control" name="alasan_cuti" required=""></textarea>
				    </div>
			        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Keluar</button>
			        <button type="submit" class="btn btn-primary">Simpan</button>
			    </form>
		  	</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	$('#datatable').dataTable();
	$(document).on('click', '.view', function(){
    let id = $(this).attr('id');
    var url = '{{route("user.dokumen.show")}}';
    $.get(url, {id:id}, function(data){
      var updateList = $('#dokumenajukanCuti');
      $(updateList).find('input[name="id"]').val(data.data.dokumen_id);
      $(updateList).find('input[name="tanggal_mulai_cuti"]').val(data.data.tanggal_mulai_cuti);
      $(updateList).find('input[name="tanggal_berakhir_cuti"]').val(data.data.tanggal_berakhir_cuti);
      $(updateList).find('textarea[name="alasan_cuti"]').html(data.data.alasan_cuti);
      $(updateList).modal('show')
    }, 'json');
  })
</script>
@endsection