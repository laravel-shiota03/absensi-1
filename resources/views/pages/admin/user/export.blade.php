<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>List</title>
	<style type="text/css">
		table tr td, table tr th{
			border: 1px solid black;
			padding: 0 10px;
		}
	</style>
</head>
<body>
	<?php 
		header("Content-Type: application/vnd-ms-excel");
		header("Content-Disposition: attachment; filename=List Data Pengguna.xls");
	?>
	<table>
		<thead>
			<tr>
				<th style="background: black; color: white;" colspan="7">
					<h1 style="text-align: center;">Karyawan PT. Vilo Gelato</h1>
				</th>
			</tr>
			<tr>
				<th colspan="7"></th>
			</tr>
		</thead>
		<thead>
			<tr>
				<th style="background: black; color: white;">No</th>
				<th style="background: black; color: white;">NIK</th>
				<th style="background: black; color: white;">Nama</th>
				<th style="background: black; color: white;">Jenis Kelamin</th>
				<th style="background: black; color: white;">Jabatan</th>
				<th style="background: black; color: white;">No. Hp</th>
				<th style="background: black; color: white;">Alamat</th>
			</tr>
		</thead>
		<tbody>
			<?php $no = 1; ?>
			@foreach ($karyawan as $key => $value)
				<tr>
					<td>{{ $no++ }}</td>
					<td>{{ $value->nik }}</td>
					<td>{{ $value->nama }}</td>
					<td>{{ $value->nama_jabatan }}</td>
					<td>{{ $value->jenis_kelamin }}</td>
					<td>{{ $value->no_hp }}</td>
					<td>{{ $value->alamat }}</td>
				</tr>
			@endforeach
		</tbody>
	</table>

</body>
</html>