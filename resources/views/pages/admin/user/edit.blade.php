@extends('pages.admin.layout')
@section('pageheader', 'Edit User')
@section('content')
<script type="text/javascript">
  document.getElementsByClassName('menu-side')[1].classList.add('active')
</script>
<div class="container">
	<div class="row">
		<h4 class="col-lg-6 col-12 my-2"><i class="fas fa-user-secret me-3"></i>{{ $karyawan->nama }}</h4>
	</div>
	<br>
	<form action="{{ route('admin.updateuser') }}" method="post">
		@csrf
	<div class="table-responsive">
		<table class="table table-striped table-bordered table-hovered">
			<tr>
				<th>Nama</th>
				<td class="text-center">:</td>
				<td><input required type="text" name="nama" value="{{ $karyawan->nama }}" class="form-control" /><input required type="hidden" name="niklama" value="{{ $karyawan->nik }}" class="form-control" /></td>
			</tr>
			<tr>
				<th>NIK</th>
				<td class="text-center">:</td>
				<td><input required type="number" name="nik" value="{{ $karyawan->nik }}" class="form-control" /></td>
			</tr>
			<tr>
				<th>Password</th>
				<td class="text-center">:</td>
				<td><input required type="password" name="password" value="{{ $karyawan->password }}" class="form-control" /></td>
			</tr>
			<tr>
				<th>Jabatan</th>
				<td class="text-center">:</td>
				<td>
					<select name="id_jabatan" class="form-control">
			    		@foreach($jabatan as $jab)
			    			<option @if($karyawan->jabatan_id == $jab->jabatan_id) selected @endif value="{{ $jab->jabatan_id }}">{{ $jab->nama_jabatan }}</option>
			    		@endforeach
			    	</select>
				</td>
			</tr>
			<tr>
				<th>Jenis Kelamin</th>
				<td class="text-center">:</td>
				<td>
					<select class="form-control" name="jenis_kelamin">
			    		<option @if($karyawan->jenis_kelamin == "Pria") selected @endif value="Pria">Pria</option>
			    		<option @if($karyawan->jenis_kelamin == "Wanita") selected @endif value="Wanita">Wanita</option>
			    	</select>
				</td>
			</tr>
			<tr>
				<th>Nomor Hp</th>
				<td class="text-center">:</td>
				<td><input type="number" name="no_hp" value="{{ $karyawan->no_hp }}" class="form-control" /></td>
			</tr>
			<tr>
				<th>Alamat</th>
				<td class="text-center">:</td>
				<td><input type="text" name="alamat" value="{{ $karyawan->alamat }}" class="form-control" /></td>
			</tr>
			<tr>
				<td colspan="3" class="text-end"><button class="btn btn-primary mt-3">Simpan</button></td>
			</tr>
		</table>
	</div>
	</form>
</div>
@endsection