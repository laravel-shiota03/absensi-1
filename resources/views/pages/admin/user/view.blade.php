@extends('pages.admin.layout')
@section('pageheader', 'View User')
@section('content')
<script type="text/javascript">
  document.getElementsByClassName('menu-side')[1].classList.add('active')
</script>
<div class="container">
	<div class="row">
		<h4 class="col-lg-6 col-12 my-2"><i class="fas fa-user-secret me-3"></i>{{ $karyawan->nama }}</h4>
	</div>
	<br>
	<div class="table-responsive">
		<table class="table table-striped table-bordered table-hovered">
			<tr>
				<th>Nama</th>
				<td class="text-center">:</td>
				<td>{{ $karyawan->nama }}</td>
			</tr>
			<tr>
				<th>NIK</th>
				<td class="text-center">:</td>
				<td>{{ $karyawan->nik }}</td>
			</tr>
			<tr>
				<th>Jabatan</th>
				<td class="text-center">:</td>
				<td>{{ $karyawan->nama_jabatan }}</td>
			</tr>
			<tr>
				<th>Jenis Kelamin</th>
				<td class="text-center">:</td>
				<td>{{ $karyawan->jenis_kelamin }}</td>
			</tr>
			<tr>
				<th>Nomor Hp</th>
				<td class="text-center">:</td>
				<td>{{ $karyawan->no_hp }}</td>
			</tr>
			<tr>
				<th>Alamat</th>
				<td class="text-center">:</td>
				<td>{{ $karyawan->alamat }}</td>
			</tr>
		</table>
	</div>
</div>
@endsection