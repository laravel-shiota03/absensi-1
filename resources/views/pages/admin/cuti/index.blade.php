@extends('pages.admin.layout')
@section('pageheader', 'Data Pengajuan Cuti Karyawan')
@section('content')
<script type="text/javascript">
  document.getElementsByClassName('menu-side')[3].classList.add('active')
</script>
<div class="container">
	<div class="row">
		<h4 class="col-lg-6 col-12 my-2"><i class="fas fa-file me-3"></i>Data Pengajuan Cuti Karyawan</h4>
	</div>
	<br>
	<div class="table-responsive">
		<table class="table table-hovered table-bordered table-striped" id="datatable">
			<thead>
				<tr>
					<th>No</th>
					<th>Nama</th>
					<th>NIK</th>
					<th>Tanggal Pengajuan</th>
					<th>Alasan Cuti</th>
					<th>Status Pengajuan</th>
					<th>Alasan Ditolak</th>
					<th>Aksi</th>
				</tr>
			</thead>
			<tbody>
				<?php $no = 1; ?>
				@foreach($karyawan as $kar)
				<tr>
					<td>{{ $no++ }}</td>
					<td>{{ $kar->nama }}</td>
					<td>{{ $kar->nik }}</td>
					<td>{{ $kar->tanggal_mulai_cuti }} - {{ $kar->tanggal_berakhir_cuti }}</td>
					<td>{{ $kar->alasan_cuti }}</td>
					<td>{{ $kar->status }}</td>
					<td>
						@if($kar->status == "Ditolak")
							{{ $kar->alasan_ditolak }}
						@endif
						<form method="post" action="{{ route('admin.tolakcuti') }}">
							@csrf
							<input type="hidden" name="dokumen_id" value="{{ $kar->dokumen_id }}">
							<div class="d-none" id="alasan-{{ $kar->dokumen_id }}">
								<label>Alasan Ditolak</label>
								<input type="text" name="alasan_ditolak" required class="form-control">
								<button class="btn btn-primary mt-2">Simpan</button>
							</div>
						</form>
					</td>
					<td>
						@if($kar->status == "Sedang Diproses")
							<a href="{{ route('admin.cuti') }}/setujui/{{ $kar->dokumen_id }}" class="view my-1 btn btn-primary">Setujui</a>
							<button onclick="ditolak({{ $kar->dokumen_id }})" class="my-1 btn btn-danger">Tolak</a>
						@elseif($kar->status == "Disetujui")
							<a href="{{ route('user.cuti') }}/file-pdf/{{ $kar->dokumen_id }}" target="__blank" class="btn btn-info">
								<i class="fas fa-file-pdf"></i>
							</a>
						@endif
					</td>
				</tr>
				@endforeach
			</tbody>
		</table>
	</div>
</div>
<script type="text/javascript">
	function ditolak(id){
		$('#alasan-'+id).removeClass('d-none')
	}
</script>
@endsection