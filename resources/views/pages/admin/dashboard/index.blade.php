@extends('pages.admin.layout')
@section('pageheader', 'Dashboard')
@section('content')
<script type="text/javascript">
  document.getElementsByClassName('menu-side')[0].classList.add('active')
</script>
<style type="text/css">
  .icon-dashboard i{
    transform: rotate(15deg);
    color: whitesmoke;
    opacity: 50%;
    font-size: 4rem
  }
</style>
<div class="container">
  <div class="row">
    <div class="col-md-4 col-12 my-2">
      <div class="bg-primary rounded text-white p-2">
        <div class="d-flex align-items-center">
          <div class="col-9 col-lg-8">
            <h4 class="text-white">Administrator</h4>
            <h1 class="text-white">{{ $administrator }}</h1>
          </div>
          <h1 class="col-3 col-lg-4 text-end icon-dashboard"><i class="fas fa-user-secret"></i></h1>
        </div>
        <hr class="my-1">
        <a href="{{ route('admin.administrator') }}" class="text-white text-decoration-none">See more <i class="fas fa-arrow-right ms-2"></i></a>
      </div>
    </div>
    <div class="col-md-4 col-12 my-2">
      <div class="bg-info rounded text-white p-2">
        <div class="d-flex align-items-center">
          <div class="col-9 col-lg-8">
            <h4 class="text-white">Karyawan</h4>
            <h1 class="text-white">{{ $karyawan }}</h1>
          </div>
          <h1 class="col-3 col-lg-4 text-end icon-dashboard"><i class="fas fa-user"></i></h1>
        </div>
        <hr class="my-1">
        <a href="{{ route('admin.karyawan') }}" class="text-white text-decoration-none">See more <i class="fas fa-arrow-right ms-2"></i></a>
      </div>
    </div>
    <div class="col-md-4 col-12 my-2">
      <div class="bg-success rounded text-white p-2">
        <div class="d-flex align-items-center">
          <div class="col-9 col-lg-8">
            <h4 class="text-white">Pengajuan Cuti</h4>
            <h1 class="text-white">{{ $cuti }}</h1>
          </div>
          <h1 class="col-3 col-lg-4 text-end icon-dashboard"><i class="fas fa-hospital"></i></h1>
        </div>
        <hr class="my-1">
        <a href="{{ route('admin.cuti') }}" class="text-white text-decoration-none">See more <i class="fas fa-arrow-right ms-2"></i></a>
      </div>
    </div>
  </div>
  <hr class="my-3">
  <div class="row">
    <div class="col-md-5 col-12">
      <form method="post" action="{{ route('admin.jam.kerja.update') }}" class="p-md-4 p-2 border rounded">
        @csrf
        <h3 class="text-center col-12"><strong>Atur Jam Kerja Karyawan</strong></h3>
        <hr class="my-2"></hr>
        <div class="col-12 text-start">
          <h4><strong>Jam Pagi</strong></h4>
        </div>
        <div class="row alert alert-secondary text-center">
          <div class="my-3 col-md-6">
            <div>
              <h4 class="my-0 mb-3"><strong>Jam Masuk</strong></h4>
              <input type="time" name="jam_masuk_pagi" class="form-control" value="{{ $jampagi->jam_masuk }}" required>
            </div>
          </div>
          <div class="my-3 col-md-6">
            <div>
              <h4 class="my-0 mb-3"><strong>Jam Pulang</strong></h4>
              <input type="time" name="jam_pulang_pagi" class="form-control" value="{{ $jampagi->jam_pulang }}" required>
            </div>
          </div>
          <div class="col-12">
            <button class="btn btn-secondary w-100" name="jampagi">Simpan</button>
          </div>
        </div>
        <div class="col-12 text-start">
          <h4><strong>Jam Siang</strong></h4>
        </div>
        <div class="row alert alert-secondary text-center">
          <div class="my-3 col-md-6">
            <div>
              <h4 class="my-0 mb-3"><strong>Jam Masuk</strong></h4>
              <input type="time" name="jam_masuk_siang" class="form-control" value="{{ $jamsiang->jam_masuk }}" required>
            </div>
          </div>
          <div class="my-3 col-md-6">
            <div>
              <h4 class="my-0 mb-3"><strong>Jam Pulang</strong></h4>
              <input type="time" name="jam_pulang_siang" class="form-control" value="{{ $jamsiang->jam_pulang }}" required>
            </div>
          </div>
          <div class="col-12">
            <button class="btn btn-secondary w-100" name="jamsiang">Simpan</button>
          </div>
        </div>
      </form>
    </div>
    <div class="col-md-7 col-12">
      <div class="p-md-4 p-2 border rounded">
        <h3 class="text-center col-12"><strong>Data Jabatan Karyawan</strong></h3>
        <button class="btn btn-success" data-bs-toggle="modal" data-bs-target="#addJabatan">
          <i class="fas fa-plus-square me-2"></i> Tambah Data
        </button>
        <br><br>
        <div class="table-responsive">
          <table id="datatable" class="table table-hovered table-bordered table-striped">
            <thead>
              <tr>
                <th>No</th>
                <th>Nama Jabatan</th>
                <th>Aksi</th>
              </tr>
            </thead>
            <tbody>
              <?php $no = 1;  ?>
              @foreach($jabatan as $jab)
                <tr>
                  <td><?= $no++ ?></td>
                  <td>{{$jab->nama_jabatan}}</td>
                  <td>
                    <button id="{{ $jab->jabatan_id }}" class="my-1 btn btn-warning edit"><i class="fas fa-edit text-white"></i></button>
                    <a href="{{ route('admin.jabatan') }}/delete/{{ $jab->jabatan_id }}" class="my-1 btn btn-danger"><i class="fas fa-trash"></i></a>
                  </td>
                </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
  <hr class="my-3">
</div>
<div class="modal fade" id="addJabatan" tabindex="-1" aria-labelledby="addJabatanLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="addJabatanLabel">Tambah Data Jabatan karyawan</h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
          </div>
          <div class="modal-body">
            <form action="{{ route('admin.jabatan.store') }}" method="post">
              @csrf
              <label>Nama Jabatan</label>
              <input type="text" name="nama_jabatan" required="" class="form-control">
              <br>
              <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Keluar</button>
              <button type="submit" name="add" class="btn btn-primary">Simpan</button>
          </form>
          </div>
      </div>
    </div>
</div>
<div class="modal fade" id="editJabatan" tabindex="-1" aria-labelledby="editJabatanLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="editJabatanLabel">Update Data Jabatan karyawan</h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
          </div>
          <div class="modal-body">
            <form action="{{ route('admin.jabatan.store') }}" method="post">
              @csrf
              <label>Nama Jabatan</label>
              <input type="hidden" id="jabatan_id" name="jabatan_id" required="" class="form-control">
              <input type="text" id="nama_jabatan" name="nama_jabatan" required="" class="form-control">
              <br>
              <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Keluar</button>
              <button type="submit" name="edit" class="btn btn-primary">Simpan</button>
          </form>
          </div>
      </div>
    </div>
</div>
<script type="text/javascript">
  $(document).on('click', '.edit', function(){
    let id = $(this).attr('id');
    var url = '{{route("admin.jabatan.show")}}';
    $.get(url, {id:id}, function(data){
      var updateList = $('#editJabatan');
      $(updateList).find('input[name="jabatan_id"]').val(data.data.jabatan_id);
      $(updateList).find('input[name="nama_jabatan"]').val(data.data.nama_jabatan);
      $(updateList).modal('show')
    }, 'json');
  })
</script>
@endsection