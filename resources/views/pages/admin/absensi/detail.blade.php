@extends('pages.admin.layout')
@section('pageheader', 'Data Absensi Karyawan')
@section('content')
<script type="text/javascript">
  document.getElementsByClassName('menu-side')[2].classList.add('active')
</script>
<div class="container">
	<a href="{{ route('admin.absensi') }}/view/{{ $tanggal }}" class="btn btn-success text-white my-1 my-lg-0"><i class="fas fa-arrow-left me-2"></i>Kembali</a>
	<br>
	<div class="row">
		<h4 class="col-lg-6 col-12 my-2"><i class="fas fa-database me-3"></i>Data Absensi {{ $karyawan->nama }}</h4>
	</div>
	<br>
	<div class="table-responsive">
		<table class="border w-100 table table-striped table-bordered table-hovered" id="datatable">
			<thead>
		        <tr>
					<th>Tanggal</th>
					<th>Shift Kerja</th>
					<th>Jam Masuk</th>
					<th>Jam Pulang</th>
					<th>Status</th>
		        </tr>
	     	</thead>
			<tbody>
				@foreach($absen as $ab)
				<tr>
					<td>{{ $ab->tanggal_absen }}</td>
					<td>@if($ab->jam_id == 1) Pagi @elseif($ab->jam_id == 2) Siang @endif</td>
					<td>{{ $ab->jam_masuk }}</td>
					<td>{{ $ab->jam_pulang }}</td>
					<td>{{ $ab->status_absen }}</td>
				</tr>
				@endforeach
			</tbody>
		</table>
	</div>
</div>
<script type="text/javascript">
	$('#datatable').dataTable();
</script>
@endsection