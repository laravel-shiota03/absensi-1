@extends('pages.admin.layout')
@section('pageheader', 'Data Absensi Karyawan')
@section('content')
<script type="text/javascript">
  document.getElementsByClassName('menu-side')[2].classList.add('active')
</script>
<div class="container">
	<a href="{{ route('admin.absensi') }}" class="btn btn-success text-white my-1 my-lg-0"><i class="fas fa-arrow-left me-2"></i>Kembali</a>
	<br>
	<div class="row">
		<h4 class="col-lg-6 col-12 my-2"><i class="fas fa-database me-3"></i>Data Absensi</h4>
		<div class="col-lg-6 col-12 my-2 text-lg-end">
			<div>
				<a target="__blank" href="{{ route('admin.absensi') }}/export/{{ $tanggal }}" class="btn btn-info text-white my-1 my-lg-0"><i class="fas fa-file-excel me-2"></i>Export</a>
			</div>
		</div>
	</div>
	<br>
	<div class="table-responsive">
		<table class="table table-hovered table-bordered table-striped" id="datatable">
			<thead>
				<tr>
					<th>No</th>
					<th>Nama</th>
					<th>NIK</th>
					<th>Tepat Waktu</th>
					<th>Izin</th>
					<th>Terlambat</th>
					<th>Aksi</th>
				</tr>
			</thead>
			<tbody>
				<?php
					$no = 1;
					$split = explode('-', $tanggal);
					$jumlahhari = cal_days_in_month(CAL_GREGORIAN, $split[1], $split[0]);
					
				?>
				@foreach($karyawan as $kar)
				@php
					$hadir = DB::table('absensis')->where('nik', $kar->nik)->whereBetween('tanggal_absen', [date($tanggal.'-01'), date($tanggal.'-'.$jumlahhari)])->where('status_absen', 'Tepat Waktu')->get()->count();

					$izin = DB::table('absensis')->where('nik', $kar->nik)->whereBetween('tanggal_absen', [date($tanggal.'-01'), date($tanggal.'-'.$jumlahhari)])->where('status_absen', 'Izin')->get()->count();

					$terlambat = DB::table('absensis')->where('nik', $kar->nik)->whereBetween('tanggal_absen', [date($tanggal.'-01'), date($tanggal.'-'.$jumlahhari)])->where('status_absen', 'Terlambat')->get()->count();
				@endphp
					<tr>
						<td>{{ $no++ }}</td>
						<td>{{ $kar->nama }}</td>
						<td class="text-center">{{ $kar->nik }}</td>
						<td class="text-center">{{ $hadir }}</td>
						<td class="text-center">{{ $izin }}</td>
						<td class="text-center">{{ $terlambat }}</td>
						<td>
							<a href="{{ route('admin.absensi') }}/detail?tanggal={{ $tanggal }}&user={{ $kar->nik }}" class="my-1 btn btn-primary">Lihat Detail</a>
						</td>
					</tr>
				@endforeach
			</tbody>
		</table>
	</div>
</div>
@endsection