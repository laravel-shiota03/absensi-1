@extends('pages.admin.layout')
@section('pageheader', 'Data Absensi Karyawan')
@section('content')
<script type="text/javascript">
  document.getElementsByClassName('menu-side')[2].classList.add('active')
</script>
<div class="container">
	<div class="row">
		<h4 class="col-lg-6 col-12 my-2"><i class="fas fa-database me-3"></i>Data Absensi</h4>
	</div>
	<br>
	<?php
		$startDate = strtotime('2023-01-01');
		$endDate = strtotime(date('Y-m-d'));
		$currentDate = $startDate;
		$no = 1;
	?>
	<div class="table-responsive">
		<h5>Pilih Bulan</h5>
		<table class="table table-hovered table-bordered table-striped" id="datatable">
			<thead>
				<tr>
					<th>No</th>
					<th>Bulan, Tahun</th>
					<th>Aksi</th>
				</tr>
			</thead>
			<tbody>
				@while($currentDate <= $endDate)
					<tr>
						<th>{{ $no++ }}</th>
						<th>{{ date('F', $currentDate) }}, {{ date('Y', $currentDate) }}</th>
						<td>
							<a href="{{ route('admin.absensi') }}/view/{{  date('Y', $currentDate)}}-{{ date('m', $currentDate) }}" class="my-1 btn btn-primary">Cek Absensi</a>
						</td>
					</tr>
					<?php $currentDate = strtotime('+1 month', $currentDate); ?>
				@endwhile
			</tbody>
		</table>
	</div>
</div>
@endsection