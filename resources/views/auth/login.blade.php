<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>PT Vilo Gelato | Login</title>
    <link rel="stylesheet" type="text/css" href="{{ asset('') }}assets/css/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="{{ asset('') }}assets/fontawesome/css/all.min.css">
    <link rel="stylesheet" type="text/css" href="{{ asset('') }}assets/iziToast/iziToast.min.css">
    <link rel="shortcut icon" type="text/css" href="{{ asset('') }}assets/images/logo.png">

    <script type="text/javascript" src="{{ asset('') }}assets/iziToast/iziToast.min.js"></script>

    <style type="text/css">
    	img.latar_belakang{
    		height: 100vh;
    		width: 100%;
    		filter: brightness(50%) blur(4px);
    	}
    	.cover_latar{
    		height: calc(100vh - 10%);
    		width: 90%;
    		top: 5%;
    		left: 5%;
    		opacity: 20%;
    		border-radius: 24px;
    	}
    	form{
    		background-color: rgb(0, 84, 165);
    	}
    	.cover_form{
    		height: 100vh;
    		width: 100%;
    	}
    	.form_outer{
    		top: 0;
    		left: 7.5%;
    		width: 85%;
    	}
    </style>
</head>
<body class="position-relative">
    <img class="latar_belakang" src="{{ asset('') }}assets/images/background.jpg">
    <div class="bg-primary position-absolute cover_latar"></div>
    <div class="position-absolute form_outer">
	    <div class="d-flex align-items-center justify-content-center cover_form">
	    	<form method="post" action="{{ route('process.login') }}" class="text-white py-4 px-md-5 px-2 rounded">
	    		@csrf
	    		<div class="header_form text-center">
		    		<h3 class="my-0 mb-1">Sistem Informasi Absensi Karyawan</h3>
		    		<h5 class="my-0 mt-1"><em>PT Vilo Gelato</em></h5>
	    		</div>
		    	<hr class="my-3">			    	
	    		<div class="form_content text-start">
	    			<div class="form-group">
	    				<label for="nip" class="mb-1"><strong>Nomor Induk Pegawai</strong></label>
	    				<div class="input-group">
	    					<input id="nip" type="number" name="nip" class="form-control" value="<?= old('nip') ?>">
	    					<span class="input-group-text"><i class="fas fa-user-circle"></i></span>
	    				</div>
	    				@error('nip')<small class="text-danger"><em>{{ $message }}</em></small>@enderror
	    			</div>
	    			<div class="form-group mt-2">
	    				<label for="password" class="mb-1"><strong>Password</strong></label>
	    				<div class="input-group">
	    					<input id="password" type="password" name="password" class="form-control">
	    					<span class="input-group-text"><i class="fas fa-key"></i></span>
	    				</div>
	    				@error('password')<small class="text-danger"><em>{{ $message }}</em></small>@enderror
	    			</div>
	    			<div class="form-group mt-3">
	    				<button class="btn btn-warning w-100"><strong>Login</strong></button>
	    			</div>
	    		</div>
	    		<hr class="my-3">
	    		<div class="footer_form text-center">
	    			<small><em>Copyright &copy; <script type="text/javascript">var tahun = new Date; document.write(tahun.getFullYear())</script> PT Vilo Gelato</em></small>
	    		</div>
	    	</form>
	    </div>
	</div>
	
	@if(Session::has('success'))
	<?=
		'<script type="text/javascript">
			iziToast.success({
            	title : "Success",
            	message: "'.Session::get('success').'",
            	position: "topCenter"
        	})
		</script>'
	?>
	@endif
	@if(Session::has('error'))
	<?=
		'<script type="text/javascript">
			iziToast.error({
            	title : "Error",
            	message: "'.Session::get('error').'",
            	position: "topCenter"
        	})
		</script>'
	?>
	@endif
</body>
</html>